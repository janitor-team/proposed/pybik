/********************************************************************************
** Form generated from reading UI file 'help.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef HELP_H
#define HELP_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_DialogHelp
{
public:
    QVBoxLayout *verticalLayout;
    QTextBrowser *text_help;

    void setupUi(QDialog *DialogHelp)
    {
        if (DialogHelp->objectName().isEmpty())
            DialogHelp->setObjectName(QStringLiteral("DialogHelp"));
        DialogHelp->resize(380, 500);
        DialogHelp->setMinimumSize(QSize(200, 200));
        verticalLayout = new QVBoxLayout(DialogHelp);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(2, 2, 2, 2);
        text_help = new QTextBrowser(DialogHelp);
        text_help->setObjectName(QStringLiteral("text_help"));
        text_help->setAcceptRichText(false);
        text_help->setOpenLinks(false);

        verticalLayout->addWidget(text_help);


        retranslateUi(DialogHelp);

        QMetaObject::connectSlotsByName(DialogHelp);
    } // setupUi

    void retranslateUi(QDialog *DialogHelp)
    {
        DialogHelp->setWindowTitle(gettext_translate("Help", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DialogHelp: public Ui_DialogHelp {};
} // namespace Ui

QT_END_NAMESPACE

#endif // HELP_H
