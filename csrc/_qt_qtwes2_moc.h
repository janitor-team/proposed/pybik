//generated from: build/temp.linux-x86_64-3.5/pybiklib/ext/qt_qtwes2.py

#include "Python.h"    //line 51
#include <QtCore/QObject>    //line 53
#include <QtCore/QVariant>    //line 54
#include <QtGui/QImage>    //line 55
#include <QtGui/QOpenGLDebugLogger>    //line 56
#include <QtGui/QPainter>    //line 58
#include <QtGui/QDesktopServices>    //line 59
#include <QtGui/QStandardItemModel>    //line 60
#include <QtWidgets/QOpenGLWidget>    //line 61
#include <QtWidgets/QMainWindow>    //line 62
#include <QtWidgets/QLineEdit>    //line 63
#include <QtWidgets/QLabel>    //line 64
#include <QtWidgets/QPushButton>    //line 65
#include <QtWidgets/QTreeView>    //line 66
#include <QtWidgets/QDialog>    //line 67
#include <QtWidgets/QListWidget>    //line 68
#include <QtWidgets/QListWidgetItem>    //line 69

class Renderer : public QObject    //line 326
{    //line 326
    Q_OBJECT    //line 326

public:    //line 326
    Renderer()    //line 328
         : QObject() {}    //line 329
    void connect_gldebuglogger(QOpenGLDebugLogger *gldebuglogger)    //line 331
    {    //line 332
        QObject::connect(gldebuglogger, SIGNAL(messageLogged(const QOpenGLDebugMessage &)), this,    //line 333
                           SLOT(on_messageLogged(const QOpenGLDebugMessage &)), Qt::DirectConnection);    //line 334
    }    //line 336

    Q_INVOKABLE void on_beforeRendering();    //line 339
    Q_INVOKABLE void on_messageLogged(const QOpenGLDebugMessage &msg);    //line 366

    Q_SIGNAL void picking_result(int);    //line 371
    Q_SIGNAL void debug_fps(int);    //line 372
    Q_SIGNAL void offscreen_image(QImage);    //line 374
};    //line 380

class SectionNameIndexItem : public QObject    //line 607
{    //line 607
    Q_OBJECT    //line 607

public:    //line 607
    QString m_section;    //line 609
    Q_SIGNAL void section_changed();    //line 609
    Q_PROPERTY (QString section MEMBER m_section NOTIFY section_changed)    //line 609
    QString m_name;    //line 610
    Q_SIGNAL void name_changed();    //line 610
    Q_PROPERTY (QString name MEMBER m_name NOTIFY name_changed)    //line 610
    int m_index;    //line 611
    Q_PROPERTY (int index MEMBER m_index)    //line 611

    SectionNameIndexItem(const QString &section, const QString &name, int index)    //line 614
    { m_section = section; m_name = name; m_index = index; }    //line 615
};    //line 616

class TextKeyItem : public QObject    //line 619
{    //line 619
    Q_OBJECT    //line 619

public:    //line 619
    QString m_text;    //line 621
    Q_SIGNAL void text_changed();    //line 621
    Q_PROPERTY (QString text MEMBER m_text NOTIFY text_changed)    //line 621
    QString m_key;    //line 622
    Q_SIGNAL void key_changed();    //line 622
    Q_PROPERTY (QString key MEMBER m_key NOTIFY key_changed)    //line 622

    TextKeyItem(const QString &text, const QString &key)    //line 625
    { m_text = text; m_key = key; }    //line 626
};    //line 627

class FacePrefsItem : public QObject    //line 630
{    //line 630
    Q_OBJECT    //line 630

public:    //line 630
    QString m_color;    //line 632
    Q_SIGNAL void color_changed();    //line 632
    Q_PROPERTY (QString color MEMBER m_color NOTIFY color_changed)    //line 632
    QString m_folder;    //line 633
    Q_SIGNAL void folder_changed();    //line 633
    Q_PROPERTY (QString folder MEMBER m_folder NOTIFY folder_changed)    //line 633
    QString m_image;    //line 634
    Q_SIGNAL void image_changed();    //line 634
    Q_PROPERTY (QString image MEMBER m_image NOTIFY image_changed)    //line 634
    QString m_imagemode;    //line 635
    Q_SIGNAL void imagemode_changed();    //line 635
    Q_PROPERTY (QString imagemode MEMBER m_imagemode NOTIFY imagemode_changed)    //line 635

    FacePrefsItem(const QString &color, const QString &folder, const QString &image, const QString &imagemode)    //line 638
    { m_color = color; m_folder = folder; m_image = image; m_imagemode = imagemode; }    //line 639
};    //line 640

class MainView : public QMainWindow    //line 1168
{    //line 1168
    Q_OBJECT    //line 1168

public:    //line 1168
    MainView()    //line 1170
         : QMainWindow() {}    //line 1171
    void connect_renderer(Renderer *renderer);    //line 1181
    void connect_renderer_offscreen(Renderer *renderer);    //line 1194
    void connect_view();    //line 1202
    void connect_sidepane(QPushButton *button, QTreeView *treeview);    //line 1212

    Q_INVOKABLE void _on_picking_result(int index);    //line 1224
    Q_INVOKABLE void _on_debug_fps(int fps);    //line 1228
    Q_INVOKABLE void _on_offscreen_image(QImage image);    //line 1236
    Q_INVOKABLE void on_beforeSynchronizing();    //line 1243
    Q_INVOKABLE void on_sceneGraphInvalidated();    //line 1263

    void resizeEvent(QResizeEvent *event);    //line 1272
    void closeEvent(QCloseEvent *event);    //line 1280
    void keyPressEvent(QKeyEvent *event);    //line 1288

    Q_INVOKABLE void on_action_challenge_triggered();    //line 1295
    Q_INVOKABLE void on_action_new_solved_triggered();    //line 1296
    Q_INVOKABLE void on_action_preferences_triggered();    //line 1297
    Q_INVOKABLE void on_action_reset_rotation_triggered();    //line 1298
    Q_INVOKABLE void on_action_rewind_triggered();    //line 1299
    Q_INVOKABLE void on_action_previous_triggered();    //line 1300
    Q_INVOKABLE void on_action_stop_triggered();    //line 1301
    Q_INVOKABLE void on_action_play_triggered();    //line 1302
    Q_INVOKABLE void on_action_next_triggered();    //line 1303
    Q_INVOKABLE void on_action_forward_triggered();    //line 1304
    Q_INVOKABLE void on_action_mark_set_triggered();    //line 1305
    Q_INVOKABLE void on_action_mark_remove_triggered();    //line 1306
    Q_INVOKABLE void on_action_initial_state_triggered();    //line 1307
    Q_INVOKABLE void on_action_edit_cube_triggered();    //line 1308
    Q_INVOKABLE void on_action_selectmodel_triggered();    //line 1312
    Q_INVOKABLE void on_action_selectmodel_back_triggered();    //line 1320
    void _listwidget_itemActivated(QListWidgetItem *item);    //line 1329
    Q_INVOKABLE void on_listwidget_itemActivated(QListWidgetItem *item);    //line 1342
    Q_INVOKABLE void on_listwidget_itemClicked(QListWidgetItem *item);    //line 1345
    Q_INVOKABLE void on_action_quit_triggered();    //line 1348
    Q_INVOKABLE void on_action_editbar_toggled(bool checked);    //line 1351
    Q_INVOKABLE void on_action_statusbar_toggled(bool checked);    //line 1355
    Q_INVOKABLE void on_action_help_triggered();    //line 1359
    Q_INVOKABLE void on_action_info_triggered();    //line 1364
    Q_INVOKABLE void on_splitter_splitterMoved(int pos, int index);    //line 1367
    Q_INVOKABLE void on_button_edit_clear_clicked();    //line 1372
    Q_INVOKABLE void on_button_edit_exec_clicked();    //line 1376
    Q_INVOKABLE void on_action_jump_to_editbar_triggered();    //line 1379
    Q_INVOKABLE void _on_button_sidepane_clicked();    //line 1383
    Q_INVOKABLE void _on_treeview_activated(const QModelIndex &index);    //line 1386
};    //line 1392

class MoveEdit : public QLineEdit    //line 1397
{    //line 1397
    Q_OBJECT    //line 1397

public:    //line 1397
    MoveEdit();    //line 1400
    void keyPressEvent(QKeyEvent *event);    //line 1406
    Q_INVOKABLE void on_returnpressed() const;    //line 1421
};    //line 1423

class DrawingArea : public QOpenGLWidget    //line 1427
{    //line 1427
    Q_OBJECT    //line 1427

public:    //line 1427
    DrawingArea()    //line 1429
         : QOpenGLWidget() {}    //line 1430
    void init();    //line 1432
    void paintGL();    //line 1441
    void resizeGL(int width, int height);    //line 1445
    void keyPressEvent(QKeyEvent *event);    //line 1449
    void mousePressEvent(QMouseEvent *event);    //line 1454
    void mouseReleaseEvent(QMouseEvent *event);    //line 1457
    void mouseMoveEvent(QMouseEvent *event);    //line 1460
    void wheelEvent(QWheelEvent *event);    //line 1463
    void dragEnterEvent(QDragEnterEvent *event);    //line 1466
    void dropEvent(QDropEvent *event);    //line 1477
};    //line 1490

class PreferencesDialog : public QDialog    //line 1494
{    //line 1494
    Q_OBJECT    //line 1494

public:    //line 1494
    QStandardItemModel *liststore_movekeys;    //line 1495
    bool liststore_blocked;    //line 1497
    QString current_facekey;    //line 1499
    QString image_dirname;    //line 1501
    PreferencesDialog(QWidget *parent)    //line 1505
         : QDialog(parent), liststore_blocked(false) {}    //line 1506
    void init();    //line 1508

    Q_INVOKABLE void on_slider_animspeed_valueChanged(int value);    //line 1518
    Q_INVOKABLE void on_button_animspeed_reset_clicked();    //line 1521
    Q_INVOKABLE void on_combobox_shader_currentIndexChanged(int value);    //line 1525
    Q_INVOKABLE void on_button_shader_reset_clicked();    //line 1528
    Q_INVOKABLE void on_combobox_samples_currentIndexChanged(int value);    //line 1532
    Q_INVOKABLE void on_button_antialiasing_reset_clicked();    //line 1537
    Q_INVOKABLE void on_checkbox_mirror_faces_toggled(bool checked);    //line 1541
    Q_INVOKABLE void on_spinbox_mirror_faces_valueChanged(double value);    //line 1545
    Q_INVOKABLE void on_button_mirror_faces_reset_clicked();    //line 1548
    Q_INVOKABLE void on_button_mousemode_quad_toggled(bool checked);    //line 1554
    Q_INVOKABLE void on_button_mousemode_ext_toggled(bool checked);    //line 1558
    Q_INVOKABLE void on_button_mousemode_gesture_toggled(bool checked);    //line 1562
    void fill_move_key_list();    //line 1567
    PyObject *get_move_key_list();    //line 1586
    Q_INVOKABLE void on_liststore_movekeys_itemChanged(QStandardItem *unused_item);    //line 1596
    Q_INVOKABLE void on_button_movekey_add_clicked();    //line 1601
    Q_INVOKABLE void on_button_movekey_remove_clicked();    //line 1607
    Q_INVOKABLE void on_button_movekey_reset_clicked();    //line 1612
    Q_INVOKABLE void _on_listview_faces_currentRowChanged(const QModelIndex &current);    //line 1617
    Q_INVOKABLE void on_button_color_clicked();    //line 1622
    Q_INVOKABLE void on_button_color_reset_clicked();    //line 1632
    Q_INVOKABLE void on_combobox_image_activated(int index);    //line 1638
    Q_INVOKABLE void on_button_image_reset_clicked();    //line 1642
    Q_INVOKABLE void on_radiobutton_tiled_toggled(bool checked);    //line 1646
    Q_INVOKABLE void on_radiobutton_mosaic_toggled(bool checked);    //line 1650
    Q_INVOKABLE void on_button_background_color_clicked();    //line 1655
    Q_INVOKABLE void on_button_background_color_reset_clicked();    //line 1664
};    //line 1667

class HelpDialog : public QDialog    //line 1753
{    //line 1753
    Q_OBJECT    //line 1753

public:    //line 1753
    HelpDialog(QWidget *parent)    //line 1755
         : QDialog(parent) {}    //line 1756
    void init(const QString &helptext);    //line 1758
};    //line 1760

class AboutDialog : public QDialog    //line 1774
{    //line 1774
    Q_OBJECT    //line 1774

public:    //line 1774
    AboutDialog(QWidget *parent)    //line 1776
         : QDialog(parent) {}    //line 1777
    void init();    //line 1779
    void showEvent(QShowEvent *unused_event);    //line 1783
    void resizeEvent(QResizeEvent *unused_event);    //line 1787
    bool eventFilter(QObject *unused_watched, QEvent *event);    //line 1791

    Q_INVOKABLE void on_text_translators_anchorClicked(const QUrl &link)    //line 1796
        { QDesktopServices::openUrl(link); }    //line 1797
    Q_INVOKABLE void on_tab_widget_currentChanged(int index);    //line 1800
    Q_INVOKABLE void on_text_license_short_anchorClicked(const QUrl &url);    //line 1804
};    //line 1806

