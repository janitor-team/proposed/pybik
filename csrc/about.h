/********************************************************************************
** Form generated from reading UI file 'about.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef ABOUT_H
#define ABOUT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_AboutDialog
{
public:
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_icon;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *label_appname;
    QLabel *label_version;
    QLabel *label_description;
    QTabWidget *tab_widget;
    QWidget *tab_about;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_copyright;
    QLabel *label_website;
    QLabel *label_translators;
    QTextBrowser *text_translators;
    QWidget *tab_contribute;
    QVBoxLayout *verticalLayout_5;
    QLabel *label_contribute;
    QWidget *tab_license;
    QVBoxLayout *verticalLayout_3;
    QTextBrowser *text_license_short;
    QTextBrowser *text_license_full;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *AboutDialog)
    {
        if (AboutDialog->objectName().isEmpty())
            AboutDialog->setObjectName(QStringLiteral("AboutDialog"));
        AboutDialog->setWindowModality(Qt::ApplicationModal);
        verticalLayout_2 = new QVBoxLayout(AboutDialog);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        label_icon = new QLabel(AboutDialog);
        label_icon->setObjectName(QStringLiteral("label_icon"));
        label_icon->setText(QStringLiteral("icon"));
        label_icon->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(label_icon);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_appname = new QLabel(AboutDialog);
        label_appname->setObjectName(QStringLiteral("label_appname"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_appname->sizePolicy().hasHeightForWidth());
        label_appname->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        font.setWeight(75);
        label_appname->setFont(font);
        label_appname->setText(QStringLiteral("appname"));
        label_appname->setTextFormat(Qt::PlainText);
        label_appname->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(label_appname);

        label_version = new QLabel(AboutDialog);
        label_version->setObjectName(QStringLiteral("label_version"));
        sizePolicy.setHeightForWidth(label_version->sizePolicy().hasHeightForWidth());
        label_version->setSizePolicy(sizePolicy);
        label_version->setText(QStringLiteral("version"));
        label_version->setTextFormat(Qt::PlainText);

        horizontalLayout->addWidget(label_version);


        verticalLayout->addLayout(horizontalLayout);

        label_description = new QLabel(AboutDialog);
        label_description->setObjectName(QStringLiteral("label_description"));
        label_description->setText(QStringLiteral("description"));
        label_description->setTextFormat(Qt::PlainText);
        label_description->setAlignment(Qt::AlignCenter);
        label_description->setWordWrap(true);

        verticalLayout->addWidget(label_description);


        horizontalLayout_2->addLayout(verticalLayout);


        verticalLayout_2->addLayout(horizontalLayout_2);

        tab_widget = new QTabWidget(AboutDialog);
        tab_widget->setObjectName(QStringLiteral("tab_widget"));
        tab_about = new QWidget();
        tab_about->setObjectName(QStringLiteral("tab_about"));
        verticalLayout_4 = new QVBoxLayout(tab_about);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        label_copyright = new QLabel(tab_about);
        label_copyright->setObjectName(QStringLiteral("label_copyright"));
        label_copyright->setText(QStringLiteral("copyright"));
        label_copyright->setTextFormat(Qt::PlainText);
        label_copyright->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        verticalLayout_4->addWidget(label_copyright);

        label_website = new QLabel(tab_about);
        label_website->setObjectName(QStringLiteral("label_website"));
        label_website->setText(QStringLiteral("website"));
        label_website->setTextFormat(Qt::RichText);
        label_website->setOpenExternalLinks(true);

        verticalLayout_4->addWidget(label_website);

        label_translators = new QLabel(tab_about);
        label_translators->setObjectName(QStringLiteral("label_translators"));
        label_translators->setTextFormat(Qt::PlainText);

        verticalLayout_4->addWidget(label_translators);

        text_translators = new QTextBrowser(tab_about);
        text_translators->setObjectName(QStringLiteral("text_translators"));
        text_translators->setOpenLinks(false);

        verticalLayout_4->addWidget(text_translators);

        tab_widget->addTab(tab_about, QString());
        tab_contribute = new QWidget();
        tab_contribute->setObjectName(QStringLiteral("tab_contribute"));
        verticalLayout_5 = new QVBoxLayout(tab_contribute);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        label_contribute = new QLabel(tab_contribute);
        label_contribute->setObjectName(QStringLiteral("label_contribute"));
        label_contribute->setTextFormat(Qt::RichText);
        label_contribute->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        label_contribute->setWordWrap(true);
        label_contribute->setOpenExternalLinks(true);

        verticalLayout_5->addWidget(label_contribute);

        tab_widget->addTab(tab_contribute, QString());
        tab_license = new QWidget();
        tab_license->setObjectName(QStringLiteral("tab_license"));
        verticalLayout_3 = new QVBoxLayout(tab_license);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        text_license_short = new QTextBrowser(tab_license);
        text_license_short->setObjectName(QStringLiteral("text_license_short"));
        text_license_short->setReadOnly(true);
        text_license_short->setAcceptRichText(false);
        text_license_short->setOpenLinks(false);

        verticalLayout_3->addWidget(text_license_short);

        text_license_full = new QTextBrowser(tab_license);
        text_license_full->setObjectName(QStringLiteral("text_license_full"));
        text_license_full->setLineWrapMode(QTextEdit::NoWrap);
        text_license_full->setAcceptRichText(false);
        text_license_full->setOpenExternalLinks(true);

        verticalLayout_3->addWidget(text_license_full);

        tab_widget->addTab(tab_license, QString());

        verticalLayout_2->addWidget(tab_widget);

        buttonBox = new QDialogButtonBox(AboutDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Close);

        verticalLayout_2->addWidget(buttonBox);


        retranslateUi(AboutDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), AboutDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), AboutDialog, SLOT(reject()));

        tab_widget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(AboutDialog);
    } // setupUi

    void retranslateUi(QDialog *AboutDialog)
    {
        AboutDialog->setWindowTitle(gettext_translate("About Pybik", Q_NULLPTR));
        label_translators->setText(gettext_translate("Translators:", Q_NULLPTR));
        tab_widget->setTabText(tab_widget->indexOf(tab_about), gettext_translate("About", Q_NULLPTR));
        tab_widget->setTabText(tab_widget->indexOf(tab_contribute), gettext_translate("Contribute", Q_NULLPTR));
        tab_widget->setTabText(tab_widget->indexOf(tab_license), gettext_translate("License", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class AboutDialog: public Ui_AboutDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // ABOUT_H
