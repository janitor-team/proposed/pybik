//generated from: build/temp.linux-x86_64-3.5/pybiklib/ext/qt_qtwes2.py

#include "_qt_qtwes2_moc.h"    //line 75

    void MainView::connect_renderer(Renderer *renderer)    //line 1181
    {    //line 1182
        QObject::connect(renderer, SIGNAL(picking_result(int)), this, SLOT(_on_picking_result(int)), Qt::QueuedConnection);    //line 1187
        QObject::connect(renderer, SIGNAL(debug_fps(int)), this, SLOT(_on_debug_fps(int)), Qt::QueuedConnection);    //line 1189
    }    //line 1191
    void MainView::connect_renderer_offscreen(Renderer *renderer)    //line 1194
    {    //line 1195
        QObject::connect(renderer, SIGNAL(offscreen_image(QImage)),    //line 1196
                         this, SLOT(_on_offscreen_image(QImage)), Qt::QueuedConnection);    //line 1197
    }    //line 1199
    void MainView::connect_view()    //line 1202
    {    //line 1203
        connect(this, SIGNAL(beforeSynchronizing()), this, SLOT(on_beforeSynchronizing()), Qt::DirectConnection);    //line 1204
        connect(this, SIGNAL(sceneGraphInvalidated()), this, SLOT(on_sceneGraphInvalidated()), Qt::DirectConnection);    //line 1206
    }    //line 1208
    void MainView::connect_sidepane(QPushButton *button, QTreeView *treeview)    //line 1212
    {    //line 1213
        connect(button, SIGNAL(clicked()), this, SLOT(_on_button_sidepane_clicked()), Qt::DirectConnection);    //line 1215
        connect(treeview, SIGNAL(activated(const QModelIndex &)), this, SLOT(_on_treeview_activated(const QModelIndex &)), Qt::DirectConnection);    //line 1217
    }    //line 1219
    MoveEdit::MoveEdit()    //line 1400
         : QLineEdit() {    //line 1401
        connect(this, SIGNAL(returnPressed()), this, SLOT(on_returnpressed()), Qt::DirectConnection);    //line 1402
        }    //line 1404
