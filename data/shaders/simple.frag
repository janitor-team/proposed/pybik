#version 120
//  Copyright © 2013-2015, 2017  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifdef GL_ES
precision mediump float;
#endif

uniform sampler2D tex;
varying vec3 color;
varying vec2 texcoord;
varying vec3 barycentric;

#ifndef GL_ES
const
#endif
vec3 bevel_color = vec3(pow(15./255., 2.2));
const vec3 invgamma = vec3(1./2.2);

void main()
{
    vec3 col_face;
    vec4 col_tex = texture2D(tex, texcoord);
    if (barycentric == vec3(0.)) {
        // unlabeled part
        col_face = bevel_color;
    } else {
        // the label
        col_face = mix(color.rgb, col_tex.rgb, col_tex.a);
    }
    
    gl_FragColor = vec4(pow(col_face, invgamma), 1.);
}

