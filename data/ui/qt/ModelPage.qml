//  Copyright © 2015-2016  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

FocusScope {
    id: root
    focus: true
    
    Rectangle {
        anchors.fill: parent
        color: palette.window
        focus: true
        
        SystemPalette { id: palette }
        
        ColumnLayout {
            id: selectlayout
            anchors.fill: parent
            focus: true
            spacing: 0
            
            ToolBar {
                Layout.fillWidth: true
                z: 1
                RowLayout {
                    anchors.fill: parent
                    ToolButton {
                        iconName: "back"
                        onClicked: { ctx.model_back(); root.Stack.view.pop() }
                    }
                    Label {
                        id: title
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignHCenter
                        text: ''
            }   }   }
            GridView {
                id: gridview
                Layout.fillWidth: true
                Layout.fillHeight: true
                focus: true
                keyNavigationWraps: true
                onModelChanged: { title.text = ctx.selectmodel_title; currentIndex = ctx.selectmodel_index }
                cellWidth: ctx.gu(128)
                cellHeight: ctx.gu(128)
                Component.onCompleted: {
                    forceActiveFocus()
                    // root.Stack.index == -1, use Timer to set model immediately afterwards
                    setmodeltimer.running = true
                }
                onWidthChanged: {
                    positionViewAtIndex(currentIndex, GridView.Center)
                    cellWidth = width/Math.floor(width/ctx.gu(128))
                }
                
                Timer {
                    id: setmodeltimer
                    interval: 0
                    onTriggered: gridview.model = ctx.model_get_modeldata(root.Stack.index)
                }
                
                delegate: Image {
                    width: gridview.cellWidth
                    height: ctx.gu(128)
                    fillMode: Image.PreserveAspectFit
                    source: modelData.key
                    Rectangle {
                        anchors.bottom: parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.margins: ctx.gu(4)
                        height: modeltext.height
                        width: modeltext.contentWidth+6
                        color: "#66000000"
                        radius: ctx.gu(4)
                        Text {
                            id: modeltext
                            anchors.bottom: parent.bottom
                            anchors.horizontalCenter: parent.horizontalCenter
                            width: ctx.gu(128-10)
                            text: modelData.text
                            color: "white"
                            wrapMode: Text.Wrap
                            textFormat: Text.PlainText
                            horizontalAlignment: Text.AlignHCenter
                    }   }
                    MouseArea {
                        hoverEnabled: true
                        anchors.fill: parent
                        anchors.margins: 1 //avoid spurious scroll if mouse moves to adjacent items
                        onEntered: {
                            if (gridview.contains(mapToItem(gridview, mouseX, mouseY)))
                                parent.GridView.view.currentIndex = index
                        }
                        onClicked: {
                            if (ctx.model_selected(root.Stack.index, index))
                                root.Stack.view.push(Qt.resolvedUrl("ModelPage.qml"))
                            else
                                root.Stack.view.pop(null)
                }   }   }
                highlight: Rectangle {
                    radius: ctx.gu(8)
                    color: "#55003070"
            }   }
        }
    }
}

