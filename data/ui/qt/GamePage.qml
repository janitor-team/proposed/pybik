//  Copyright © 2015-2017  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.2

FocusScope {
    id: root
    
    Component.onCompleted: drawingarea.forceActiveFocus()
    
    SystemPalette { id: palette }
    
    MouseArea {
        z: 10
        visible: ctx.messagebox_visible
        anchors.fill: parent
        cursorShape: Qt.ArrowCursor
        hoverEnabled: true
        
        property var prevFocusItem
        onVisibleChanged: { if (visible) { prevFocusItem = Window.activeFocusItem; forceActiveFocus() } }
        function restorefocus() {
            ctx.messagebox_visible = false; ctx.messagetext = ""
            if (prevFocusItem) prevFocusItem.forceActiveFocus()
        }
        onClicked: restorefocus()
        Keys.onPressed: restorefocus()
    }
    
    // !! actions with shortcut are available from other pages if not disabled
    Action {
        id: action_new_solved
        iconSource: Qt.resolvedUrl(ctx.config("UI_DIR"))+"/qt/images/new-solved.png"
        text: ctx.tr_tooltip("Ne&w Solved")
        enabled: root.visible
        shortcut: "Ctrl+Shift+N"
        onTriggered: ctx.action("new_solved")
    } Action {
        id: action_challenge
        iconSource: Qt.resolvedUrl(ctx.config("UI_DIR"))+"/qt/images/new-random.png"
        text: ctx.tr_tooltip("&New Challenge")
        enabled: root.visible
        shortcut: "Ctrl+N"
        onTriggered: ctx.action("challenge")
    } Action {
        id: action_selectmodel
        iconName: "back"
        text: ctx.tr_tooltip("Select Puzzle")
        enabled: root.visible
        shortcut: ctx.get_settings("action.selectmodel")
        onTriggered: { ctx.model_start_selection(); push(Qt.resolvedUrl("ModelPage.qml")) }
    } Action {
        id: action_quit
        iconName: "application-exit"
        text: ctx.tr("&Quit")
        shortcut: "Ctrl+Q"  // this action is available even if page not visible
        onTriggered: ctx.action("quit")
    } Action {
        id: action_initial_state
        text: ctx.tr("&Set as Initial State")
        enabled: root.visible
        shortcut: ctx.get_settings("action.initial_state")
        onTriggered: ctx.action("initial_state")
    } Action {
        id: action_reset_rotation
        text: ctx.tr("&Reset Rotation")
        enabled: root.visible
        shortcut: ctx.get_settings("action.reset_rotation")
        onTriggered: ctx.action("reset_rotation")
    } Action {
        id: action_preferences
        iconName: "document-properties"
        text: ctx.tr("&Preferences …")
        enabled: root.visible
        shortcut: ctx.get_settings("action.preferences")
        onTriggered: ctx.action("preferences")
    } Action {
        id: action_rewind
        iconName: "media-seek-backward"
        enabled: ctx.toolbarstate[0]
        onTriggered: ctx.action("rewind")
    } Action {
        id: action_previous
        iconName: "media-skip-backward"
        enabled: ctx.toolbarstate[0]
        onTriggered: ctx.action("previous")
    } Action {
        id: action_stop
        iconName: "media-playback-stop"
        onTriggered: ctx.action("stop")
    } Action {
        id: action_play
        iconName: "media-playback-start"
        enabled: ctx.toolbarstate[1]
        onTriggered: ctx.action("play")
    } Action {
        id: action_next
        iconName: "media-skip-forward"
        enabled: ctx.toolbarstate[1]
        onTriggered: ctx.action("next")
    } Action {
        id: action_forward
        iconName: "media-seek-forward"
        enabled: ctx.toolbarstate[1]
        onTriggered: ctx.action("forward")
    } Action {
        id: action_mark_set
        iconName: "list-add"
        text: ctx.tr_tooltip("Add Mark")
        tooltip: ctx.tr_tooltip("Mark the current place in the sequence of moves")
        enabled: ctx.toolbarstate[2]
        onTriggered: ctx.action("mark_set")
    } Action {
        id: action_mark_remove
        iconName: "list-remove"
        text: ctx.tr_tooltip("Remove Mark")
        tooltip: ctx.tr_tooltip("Remove the mark at the current place in the sequence of moves")
        enabled: ctx.toolbarstate[2]
        onTriggered: ctx.action("mark_remove")
    } Action {
        id: action_editbar
        text: ctx.tr("&Edit Bar")
        checkable: true
        checked: ctx.get_settings("window.editbar")
        onCheckedChanged: ctx.set_settings("window.editbar", checked)
    } Action {
        id: action_statusbar
        text: ctx.tr("&Status Bar")
        checkable: true
        checked: ctx.get_settings("window.statusbar")
        onCheckedChanged: ctx.set_settings("window.statusbar", checked)
    } Action {
        id: action_info
        iconName: "help-about"
        text: ctx.tr("&Info …")
        onTriggered: ctx.aboutdialog_visible = true
    } Action {
        id: action_help
        iconName: "help"
        text: ctx.tr("&Help …")
        enabled: root.visible
        shortcut: "F1"
        tooltip: ctx.tr("Help")
        onTriggered: ctx.helpdialog_visible = true
    } Action {
        id: action_edit_moves
        enabled: editbar.visible
        shortcut: ctx.get_settings("action.edit_moves")
        onTriggered: editbartext.forceActiveFocus()
    } Action {
        id: action_edit_cube
        enabled: root.visible
        shortcut: ctx.get_settings("action.edit_cube")
        onTriggered: ctx.action("edit_cube")
    }
    
    ToolBar {
        id: toolbar
        z: 1
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        onWidthChanged: {
            if (toolbar.width) {
                var buttonwidth = tool_forward.x - tool_next.x
                var minwidth1 = tool_forward.x + 2*buttonwidth
                if (minwidth1 < toolbar.width) {
                    if (tool_mark_set.parent != toolbarlayout1) {
                        toolbar_fillwidth.parent = null
                        tool_mark_set.parent = toolbarlayout1
                        tool_mark_remove.parent = toolbarlayout1
                        toolbar_fillwidth.parent = toolbarlayout1
                    }
                    tool_menu.parent = minwidth1 + buttonwidth < toolbar.width ? toolbarlayout1 : toolbarlayout2
                } else {
                    if (tool_mark_set.parent != toolbarlayout2) {
                        tool_menu.parent = null
                        tool_mark_set.parent = toolbarlayout2
                        tool_mark_remove.parent = toolbarlayout2
                    }
                    tool_menu.parent = toolbarlayout2
        }   }   }
        
        Keys.onPressed: event.accepted = ctx.key_pressed(event.key, event.modifiers, false)
        
        RowLayout {
            id: toolbarlayout1
            anchors.fill: parent
            ToolButton { action: action_selectmodel }
            ToolButton { action: action_rewind }
            ToolButton { action: action_previous }
            ToolButton { action: action_stop; visible: ctx.toolbarstate[3] }
            ToolButton { action: action_play; visible: !ctx.toolbarstate[3] }
            ToolButton { id: tool_next; action: action_next }
            ToolButton { id: tool_forward; action: action_forward }
            ToolButton { id: tool_mark_set; action: action_mark_set; visible: ctx.toolbarstate[4] }
            ToolButton { id: tool_mark_remove; action: action_mark_remove; visible: !ctx.toolbarstate[4] }
            Item { id: toolbar_fillwidth; Layout.fillWidth: true }
            SimpleMenuButton {
                id: tool_menu
                model: [
                    action_initial_state,
                    action_reset_rotation,
                    action_preferences,
                    action_editbar,
                    action_statusbar,
                    action_help,
                    action_info,
                ]
    }   }   }
    Rectangle {
        id: editbar
        visible: action_editbar.checked
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: toolbar.bottom
        height: visible ? implicitHeight : 0
        implicitHeight: editbartext.implicitHeight
        
        Keys.onPressed: event.accepted = ctx.key_pressed(event.key, event.modifiers, false)
        
        RowLayout {
            anchors.fill: parent
            spacing: 0
            TextField {
                id: editbartext
                Layout.fillWidth: true
                text: ""
                onEditingFinished: ctx.editing_finished(text, cursorPosition)
                Binding on text { value: ctx.edittext }
                Binding on cursorPosition { value: ctx.editposition }
            }
            ToolButton {
                id: editbarClear
                implicitWidth: editbartext.implicitHeight-4
                implicitHeight: editbartext.implicitHeight-4
                iconName: "edit-clear"
                onClicked: {editbartext.text = ""; ctx.editing_finished("", 0)}
    }   }   }
    SplitView {
        id: splitview
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: editbar.bottom
        anchors.bottom: parent.bottom
        orientation: Screen.primaryOrientation==Qt.PortraitOrientation ? Qt.Vertical : Qt.Horizontal
        
        Keys.onPressed: event.accepted = ctx.key_pressed(event.key, event.modifiers, false)
        
        handleDelegate: Rectangle {
            width: ctx.gu(12)
            height: ctx.gu(12)
            color: palette.window
            Rectangle {
                anchors.centerIn: parent
                width: parent.width / 4
                height: parent.height / 4
                color: palette.mid
            }
        }
        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumWidth: 200
            Layout.minimumHeight: 200
            Text {
                visible: ctx.debugmsg != ""
                x: 4
                y: 4
                color: "black"
                text: ctx.debugmsg
                textFormat: Text.PlainText
            }
            DropArea {
               anchors.fill: drawingarea
                onEntered: {
                    if (drag.hasColor)  drag.acceptProposedAction()
                    else if (drag.hasUrls && drag.urls[0].split(":")[0]=="file")  drag.acceptProposedAction()
                }
                onDropped: {
                    if (drop.hasColor)  ctx.drag_dropped_color(drop.x, drawingarea.height-drop.y, drop.colorData)
                    else if (drop.hasUrls)  ctx.drag_dropped_url(drop.x, drawingarea.height-drop.y, drop.urls[0])
                }
            }
            PinchArea {
                z: -1
                anchors.fill: drawingarea
                onPinchUpdated: {
                    ctx.drawingarea_zoom((pinch.previousScale - pinch.scale)*10)
                }
                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                    hoverEnabled: true
                    onEntered: ctx.drawingarea_mouse_entered(true)
                    onExited: ctx.drawingarea_mouse_entered(false)
                    onPressed: ctx.drawingarea_mouse_pressed(mouse.modifiers, mouse.button, mouse.x, mouse.y, drawingarea.height)
                    onReleased: ctx.drawingarea_mouse_released(mouse.x, mouse.y)
                    onPositionChanged: ctx.drawingarea_mouse_moved(mouse.buttons, mouse.x, mouse.y, drawingarea.height, ctx.gu(4))
                    onWheel: ctx.drawingarea_zoom(wheel.angleDelta.y/120.0)
                }
            }
            // evasive toolbar
            Row {
                id: toolbarlayout2
                z: 1
                anchors.right: parent.right
                anchors.top: parent.top
                
                Keys.onPressed: event.accepted = ctx.key_pressed(event.key, event.modifiers, false)
            }
            // This is the hole where the puzzle is visible, rendered by signal beforeRendering
            Item {
                id: drawingarea
                anchors.fill: parent
                focus: true
                activeFocusOnTab: true
                
                onWidthChanged: ctx.drawingarea_width_changed(width)
                onHeightChanged: ctx.drawingarea_height_changed(height + statusbar.height)
                Binding {
                    target: ctx
                    property: "gl_y"
                    value: root.height - toolbar.height - editbar.height - drawingarea.height
                }
                NumberAnimation {
                    id: animation
                    target: ctx
                    property: "animation_angle"
                    running: false
                    to: ctx.animation_maxangle
                    duration:  ctx.animation_maxangle / ctx.animation_speed * 100
                    onStopped: ctx.animation_stopped()
                }
                Connections {
                    target: ctx
                    onAnimation_angleChanged: { if (animation.running) ctx.animation_step(ctx.animation_angle) }
                    onAnimation_runningChanged: { animation.running = ctx.animation_running }
                }
                Keys.onPressed: event.accepted = ctx.key_pressed(event.key, event.modifiers, true)
                
                // solved message, etc.
                Rectangle {
                    visible: ctx.messagebox_visible
                    anchors.centerIn: parent
                    color: palette.button
                    width: Math.min(parent.width - ctx.gu(40), ctx.gu(320))
                    height: messagebox_text.paintedHeight + ctx.gu(16)
                    border.width: 1
                    border.color: palette.buttonText
                    radius: ctx.gu(6)
                    Text {
                        id: messagebox_text
                        color: palette.buttonText
                        anchors.margins: ctx.gu(12)
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.left: parent.left
                        anchors.right: messagebox_x.left
                        text: ctx.messagetext
                        textFormat: Text.PlainText
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignHCenter
                    }
                    Text {
                        id: messagebox_x
                        anchors.right: parent.right
                        anchors.rightMargin: ctx.gu(4)
                        text: "×"
            }   }   }
            // bottom bar, challenges, status text
            Rectangle {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                height: bottomlayout.implicitHeight
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "transparent" }
                    GradientStop { position: 1.0; color: "white" }
                }
                RowLayout {
                    id: bottomlayout
                    anchors.fill: parent
                    ToolButton { action: action_new_solved }
                    ToolButton { action: action_challenge }
                    Item {
                        id: statusbar
                        Layout.fillWidth: true
                        visible: action_statusbar.checked
                        anchors.bottom: parent.bottom
                        height: statustext.height + 4
                        Label {
                            id: statustext
                            anchors.horizontalCenter: parent.horizontalCenter
                            x: 2
                            y: 2
                            text: ctx.status
                            textFormat: Text.PlainText
                            elide: Text.ElideRight
                    }   }
                    ToolButton {
                        iconName: splitview.orientation == Qt.Horizontal ?
                                  (sidepane.visible2 ? "go-right" : "go-left") :
                                  (sidepane.visible2 ? "go-down" : "go-up")
                        onClicked: { sidepane.state = sidepane.visible ? "hidden" : "visible" }
        }   }   }   }
        Rectangle {
            id: sidepane
            visible: false
            property bool visible2: false
            property real size: ctx.get_settings("window.sidepane_width")
            Binding on width {
                value: sidepane.visible ? sidepane.size : 0
                when: splitview.orientation == Qt.Horizontal
            }
            Binding on height {
                value: sidepane.visible ? sidepane.size : 0
                when: splitview.orientation == Qt.Vertical
            }
            onWidthChanged: {
                if (visible2 && splitview.orientation == Qt.Horizontal)
                    ctx.set_settings("window.sidepane_width", width)
            }
            onHeightChanged: {
                if (visible2 && splitview.orientation == Qt.Vertical)
                    ctx.set_settings("window.sidepane_width", height)
            }
            state: "hidden"
            states : [
                State {
                    name: "hidden"
                    PropertyChanges { target: sidepane; size: 0 }
                },
                State {
                    name: "visible"
                    PropertyChanges { target: sidepane; size: ctx.get_settings("window.sidepane_width") }
                }
            ]
            transitions: [
                Transition {
                    from: "hidden"
                    to: "visible"
                    SequentialAnimation {
                        PropertyAction { target: sidepane; property: "visible"; value: true }
                        NumberAnimation { target: sidepane; property: "size"; duration: 100 }
                        PropertyAction { target: sidepane; property: "visible2"; value: true }
                }   },
                Transition {
                    from: "visible"
                    to: "hidden"
                    SequentialAnimation {
                        PropertyAction { target: sidepane; property: "visible2"; value: false }
                        NumberAnimation { target: sidepane; property: "size"; duration: 100 }
                        PropertyAction { target: sidepane; property: "visible"; value: false }
                }   }
            ]
            Component {
                id: sidepaneSection
                Rectangle {
                    width: sidepane.width
                    height: sidepaneSectionText.height
                    color: palette.button
                    Layout.minimumWidth: sidepaneSectionText.width
                    Text {
                        id: sidepaneSectionText
                        text: section
                        width: parent.width
                        horizontalAlignment: Text.AlignHCenter
                        elide: Text.ElideRight
                        color: palette.buttonText
                        font.bold: true
                        MouseArea { anchors.fill: parent }
            }   }   }
            TableView {
                anchors.fill: parent
                model: ctx.sidepanemodel
                headerVisible: false
                alternatingRowColors: false
                section.property: "modelData.section"
                section.criteria: ViewSection.FullString
                section.delegate: sidepaneSection
                section.labelPositioning: ViewSection.CurrentLabelAtStart
                                        | ViewSection.InlineLabels
                                        | ViewSection.NextLabelAtEnd
                TableViewColumn { role: "name" }
                
                onActivated: ctx.plugin_activated(ctx.sidepanemodel[row].index)
            }
        }
    }
}


