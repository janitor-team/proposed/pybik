//  Copyright © 2016  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

FocusScope {
    id: root
    property var tabs: ["TabGraphic.qml", "TabMouse.qml", "TabKeys.qml", "TabAppearance.qml"]
    
    ToolBar {
        id: toolbar
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        RowLayout {
            anchors.fill: parent
            ToolButton {
                Layout.fillHeight: true;
                iconName: "back"
                onClicked: { ctx.preferences_visible = false; root.Stack.view.pop() }
            }
            ToolButton {
                Layout.fillHeight: true;
                text: ctx.tr("Graphic")
                onClicked: tabloader.source = tabs[0]
            }
            ToolButton {
                Layout.fillHeight: true
                text: ctx.tr("Mouse")
                onClicked: tabloader.source = tabs[1]
            }
            ToolButton {
                Layout.fillHeight: true
                text: ctx.tr("Keys")
                onClicked: tabloader.source = tabs[2]
            }
            ToolButton {
                Layout.fillHeight: true
                text: ctx.tr("Appearance")
                onClicked: tabloader.source = tabs[3]
            }
            Item { Layout.fillWidth: true }
        }
    }
    Rectangle {
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: toolbar.bottom
        anchors.bottom: parent.bottom
        color: palette.window
        Loader {
            id: tabloader
            active: true
            anchors.fill: parent
            anchors.margins: ctx.gu(6)
            source: tabs[ctx.preferences_tabindex]
    }   }
    
    Keys.onPressed: {
        if (event.key == Qt.Key_Escape || event.key == Qt.Key_Return || event.key == Qt.Key_Enter) {
            event.accepted = true
            ctx.preferences_visible = false
            Stack.view.pop()
    }   }
}

