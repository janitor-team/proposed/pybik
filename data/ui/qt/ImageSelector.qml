//  Copyright © 2015-2016  B. Clausius <barcc@gmx.de>
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.2

Rectangle {
    id: root
    
    width: item.width + 6
    height: item.height + 6
    radius: 4
    border.width: 1
    border.color: palette.shadow
    
    property string currentfolder
    property var selectedvalue: ""
    property var selectedvaluev
    property var completed: false
    property var selecteditem: null
    property var selecteditemrect: Qt.rect(0,0,50,50)
    property var selectfilerect: Qt.rect(0,0,50,50)
    property var selectedrect: selecteditem ? selecteditemrect : selectfilerect
    signal image_selected(string name)
    signal imagefile_selected(url url)
    
    onSelectedvalueChanged: {
        if (completed)
            selectedvaluev = selectedvalue
    }
    Component.onCompleted: {
        completed = true
        selectedvaluev = selectedvalue
    }
    
    function set_selected(selected, item) {
        if (selected) {
            selecteditem = item
            selecteditemrect = item.mapToItem(root, 0, 0, item.width, item.height)
        } else if (selecteditem == item) {
            selecteditem = null
        }
    }
    
    Rectangle {
        x: selectedrect.x-3
        y: selectedrect.y-3
        z: 1
        width: selectedrect.width+6
        height: selectedrect.height+6
        radius: 4
        color: "#100000f0"
        border.width: 2
    }
    
    Column {
        id: item
        x: 3
        y: 3
        spacing: -1
        
        SystemPalette { id: palette }
        
        Loader {
            id: filedialogLoader
            sourceComponent: Component {
                FileDialog {
                    title: ctx.tr("Open Image")
                    onAccepted: { root.imagefile_selected(fileUrl); filedialogLoader.active = false }
                    onRejected: { filedialogLoader.active = false }
                    onVisibleChanged: { if (visible) folder = Qt.resolvedUrl(root.currentfolder) }
        }   }   }
        
        Rectangle {
            id: rect_no_image
            width: image_grid.width
            height: label_no_image.height+6
            border.width: 1
            border.color: palette.shadow
            color: palette.base
            property bool selected: selectedvaluev==""
            onSelectedChanged: set_selected(selected, rect_no_image)
            
            Label {
                id: label_no_image
                anchors.horizontalCenter: parent.horizontalCenter
                y: 3
                color: palette.text
                text: ctx.tr("no image")
            }
            MouseArea {
                anchors.fill: parent
                onClicked: root.image_selected("")
        }   }
        Grid {
            id: image_grid
            spacing: -1
            Repeater {
                model: ctx.imagemodel
                Rectangle {
                    id: rect_grid_image
                    width: 34; height: 34
                    border.width: 1
                    border.color: palette.shadow
                    color: palette.base
                    property bool selected: selectedvaluev==modelData.text
                    onSelectedChanged: set_selected(selected, rect_grid_image)
                    
                    Image {
                        id: grid_image
                        x: 1; y: 1
                        source: Qt.resolvedUrl(modelData.text)
                        sourceSize.width: 32
                        MouseArea {
                            anchors.fill: parent
                            onClicked: root.image_selected(modelData.key)
        }   }   }   }   }
        Rectangle {
            id: rect_file_image
            width: image_grid.width
            height: label_select_file.height+6
            border.width: 1
            border.color: palette.shadow
            color: palette.base
            property bool selected: selecteditem==null
            onSelectedChanged: selectfilerect = mapToItem(root, 0, 0, rect_file_image.width, rect_file_image.height)
            
            Label {
                id: label_select_file
                anchors.horizontalCenter: parent.horizontalCenter
                y: 3
                color: palette.text
                text: ctx.tr("select file")
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    filedialogLoader.active = true
                    filedialogLoader.item.visible = true
                }
}   }   }   }

