pybik (3.0-4) UNRELEASED; urgency=medium

  * Use debhelper-compat instead of debian/compat.

 -- Ondřej Nový <onovy@debian.org>  Sat, 20 Jul 2019 00:48:57 +0200

pybik (3.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python3-Version field

  [ SVN-Git Migration ]
  * Update Vcs fields for git migration

  [ B. Clausius ]
  * Standards-Version updated to 4.1.5, no changes needed
  * Remove csrc directory, should not be used during build,
    fixes build with Python 3.7

 -- B. Clausius <barcc@gmx.de>  Fri, 06 Jul 2018 22:16:42 +0200

pybik (3.0-2) unstable; urgency=medium

  * Fix build on OpenGLES architectures armel and armhf

 -- B. Clausius <barcc@gmx.de>  Thu, 26 Oct 2017 12:33:18 +0200

pybik (3.0-1) unstable; urgency=medium

  * New upstream release
    + New puzzles: Octahedron and Dodecahedron
    + Improved User-Interface
    + Support for OpenGLES v2
    + Misc improvements
    + Updated translations
  * OpenGLES v2 on armel and armhf
    + Added Build-Depends on libgles2-mesa-dev
    + Select GL variant in debian/rules
  * PyQt5 no longer used by pybik,
    it uses the qt libs from within extension modules
    + Changed (Build-)Depends
  * intltool removed from Build-Depends,
    msgfmt from package gettext (>= 0.19.6) is now used instead
  * Build-Depends help2man replaced by python3-docutils,
    the manpage is now generated with rst2man from a template
  * Standards-Version updated to 4.1.1
    + debian/upstream/signing-key.asc instead
      of the binary *.pgp (4.1.0)
    + copyright-format URL changed to https (4.0.0)
    + The menu system is deprecated (3.9.8),
      removed debian/pybik.menu (Closes: #738007)
  * Changed debhelper version and compat level to 10
  * override_dh_builddeb removed: xz compression is now the default
  * Install NEWS as upstream changelog
  * Enable the bindnow hardening flag
  * Updated debian/watch to version 4

 -- B. Clausius <barcc@gmx.de>  Mon, 23 Oct 2017 19:49:00 +0200

pybik (2.1-1) unstable; urgency=medium

  * New upstream release
    + New puzzles: Triangular Prisms and Pentagonal Prisms
    + For each puzzle type, the game is saved independently
    + Misc improvements
    + Updated translations
  * Suggests gconf2 removed:
    Settings migration from Pybik 0.5 is not supported any more
  * Changes for QGLWidget -> QOpenGLWidget transition
    + python3-pyqt5.qtopengl dependency removed
    + needs Qt (and PyQt) version 5.4
  * Compatibility with the --parallel option introduced in Python 3.5:
    As before, only the model data is generated in parallel, not the
    extension modules.
  * Reproducible builds:
    Output of sys.argv is not embedded any more by
    buildlib/translation_from_ui.py
  * Removed autopkgtest

 -- B. Clausius <barcc@gmx.de>  Fri, 31 Jul 2015 05:47:15 +0200

pybik (2.0-1) unstable; urgency=medium

  * New upstream release
    + New puzzle: Tetrahedron
    + New solution: Beginner's method
    + Added move transformations
    + Other new and improved plugins
    + Improved rendering engine
    + Added simple help
  * Updated dependencies:
    + PyQt4 -> PyQt5
    + Needs Python 3.4
    + numpy and libglm-dev removed
  * Updated Standards-Version to 3.9.6, no changes needed
  * Experimental support for reproducible builds
    + Release date in the man-page
    + Pickled data in /usr/share/pybik/models/*
  * Build without setuptools which is now standard in setup.py
  * Removed patch, applied upstream

 -- B. Clausius <barcc@gmx.de>  Tue, 28 Apr 2015 19:13:13 +0200

pybik (1.1.1-1) unstable; urgency=medium

  * New upstream release
    + New and updated translations
    + Misc bugfixes and improvements
  * Cryptographic verification of upstream packages
  * Added DEP12 upstream metadata
  * Removed patches (included upstream)
  * Added patch to fix option for pickle protocol
  * Use a pickle protocol compatible with all supported Python 3 versions
  * No need to remove __pycache__ directories in debian/rules any more
  * autopkgtest needs dependency on xauth

 -- B. Clausius <barcc@gmx.de>  Sun, 20 Jul 2014 15:56:10 +0200

pybik (1.1-2) unstable; urgency=low

  [ Martin Pitt ]
  * Use xvfb in autopkgtest. (Closes: #717126)
  * Only run the "min" and "preferences" tests in autopkgtest, as the full
    test suite takes way too long (and might not even succeed) under xvfb.

  [ Scott Kitterman ]
  * Add python3:Depends for pybik-bin since it builds python3 version specific
    files
  * Override dh_python3 and call dh_python3 a second time to look in
    pybik-bin's private dir
  * Add build-dep on dh-python (>= 1.20130901-1) for pybuild (this is the
    lowest version that will generate correct depends for pybik bin)

  [ B. Clausius ]
  * Switch to pybuild
  * Add menu file
  * Added patches from upstream:
    + Heavily reduce the size of the binary package pybik:
      lp-pybik-r2126_reduce-model-size.patch
      lp-pybik-r2127_reduce-model-size.patch
    + Fix test for newer Python version:
      lp-pybik-r2308_fix-tests-for-newer-python.patch
  * Updated Standards-Version to 3.9.5, no changes needed

 -- B. Clausius <barcc@gmx.de>  Thu, 14 Nov 2013 08:21:46 +0100

pybik (1.1-1) unstable; urgency=low

  * New upstream release
      + Rendering engine now uses modern OpenGL
        - should be faster on most systems
        - improved lighting effect
      + New and updated translations
  * Updated for Python 2 -> Python 3 transition
  * Pybik no longer works with PySide
  * Recommends python3-icu
  * Added autopkgtest

 -- B. Clausius <barcc@gmx.de>  Mon, 17 Jun 2013 20:55:24 +0200

pybik (1.0.1-2) unstable; urgency=low

  [ B. Clausius ]
  * Upload to unstable (Closes: #710606)
  * Change binary package compression to xz

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.
  * Use empty lines to separate paragraphs in debian/control.

 -- B. Clausius <barcc@gmx.de>  Sat, 01 Jun 2013 16:13:31 +0200

pybik (1.0.1-1) experimental; urgency=low

  * New upstream release
    + Improved user interface.
    + Added Towers and Bricks (non cubic puzzles).
    + Added an option to show the back faces.
    + The cube can be manipulated with the keyboard.
    + Animation is faster and rendering more beautiful.
    + Added more pretty patterns.
    + Added a new solver.
    + Added new translations.
  * More generic watch file based on the proposal by Bart Martens
  * Updated debhelper dependency and compat to 9
  * Updated Standards-Version to 3.9.4, no changes needed
  * debian/copyright:
    + Updated Format URL for final copyright format 1.0
    + Added paragraphs for image files
  * Updated Build-Depends: new: python-numpy, python-qt4, help2man
  * Updated Depends for transitions:
    + GTK2/GConf -> Qt4 (PySide or PyQt4)
    + GtkGlExt -> QtOpenGL (PySide or PyQt4)
  * Suggests python-opengl (unusual usage) and gconf2 (config transition)
  * Splittet into an arch dependent and an arch independent package
    (increased size and build time)
  * Enabled parallel build for the architecture independent part
  * Install autogenerated README file without install paragraph
  * Replace the license file (displayed in the about box) by a link
    to usr/share/common-licenses/GPL-3 and add lintian override

 -- B. Clausius <barcc@gmx.de>  Sun, 03 Feb 2013 17:35:32 +0100

pybik (0.5-1) unstable; urgency=low

  * New upstream release.
    + New solutions:
      - Spiegel improved
      - Solution for the 2×2×2 Cube
    + New file format for solutions
    + Improved mouse control
      - Mouse button with control key rotates the whole cube
      - Right button moves in the opposite direction
    + Changeable background color

 -- B. Clausius <barcc@gmx.de>  Sun, 08 Jan 2012 04:55:40 +0100

pybik (0.4.2-1) unstable; urgency=low

  * Initial release (Closes: #647390, LP: #806433)

 -- B. Clausius <barcc@gmx.de>  Mon, 28 Nov 2011 23:22:50 +0100
