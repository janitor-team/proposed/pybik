Pybik 3.0 (2017-10-22):

  * New puzzles: Octahedron and Dodecahedron
  * Improved User-Interface
  * Support for OpenGLES v2
  * Misc improvements
  * Complete translations:
        English (AE and BE), Galician, German, Malay, Ukrainian
  * Partial translations:
        French, Italian, Finnish, Russian, Spanish, Uzbek, Hebrew, Greek,
        Asturian, Bulgarian, Czech, Bosnian, Polish, Portuguese (Brazil),
        Chinese (Taiwan), Kabyle

Pybik 2.1 (2015-07-29):

  * New puzzles: Triangular Prisms and Pentagonal Prisms
  * For each puzzle type, the game is saved independently
  * Misc improvements
  * Updated translations

Pybik 2.0 (2015-04-27):

  * New puzzle: Tetrahedron
  * New solution: Beginner's method
  * Added move transformations
  * Other new and improved plugins
  * Improved rendering engine
  * Added simple help

Pybik 1.1.1 (2014-02-11):

  * New and updated translations
  * Misc bugfixes and improvements

Pybik 1.1 (2013-06-16):

  * Rendering engine now uses modern OpenGL
    - should be faster on most systems
    - improved lighting effect
  * New and updated translations

Pybik 1.0.1 (2013-02-02):

  * Minor improvements and bugfixes
  * Updated translations

Pybik 1.0 (2013-01-08):

  * Improved user interface.
  * Added Towers and Bricks (non cubic puzzles).
  * Added an option to show the back faces.
  * The cube can be manipulated with the keyboard.
  * Animation is faster and rendering more beautiful.
  * Added more pretty patterns.
  * Added a new solver.
  * Added new translations.

Pybik 0.5 (2012-01-06):

  * New solutions:
    - Spiegel improved
    - Solution for the 2×2×2 Cube
  * New file format for solutions
  * Improved mouse control
    - Mouse button with control key rotates the whole cube
    - Right button moves in the opposite direction
  * Changeable background color

Pybik 0.4 (2011-07-06):

  This release does not contain many new features. Most of the changes took
  place under the hood. A new solution (Spiegel) has been added.

Pybik 0.3 (2009-12-17):

  * Text field to edit moves in a notation similar to Singmaster's
  * The state of the cube and the moves are now stored between sessions
  * Some sequences for pretty patterns in the sidebar

Pybik 0.2 (2009-08-24):

  * Preferences are stored with GConf
  * UI Improvements and bugfixes

Pybik 0.1 (2009-08-06):

  * Port from gnubik-2.3 to Python
  * Sidebar for Actions
  * Redesigned color dialog
  * Improvements in cube rendering
