# -*- coding: utf-8 -*-

#  Copyright © 2015  B. Clausius <barcc@gmx.de>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


def invert(game):
    game.move_sequence.invert()
    game.recalc_current_state()
    
def normalize_complete_rotations(game):
    game.move_sequence.normalize_complete_rotations(game.initial_state.model)
    game.recalc_current_state()
    
def normalize_moves(game):
    game.move_sequence.normalize_moves(game.initial_state.model)
    game.recalc_current_state()
    

