# -*- coding: utf-8 -*-

#  Copyright © 2017  B. Clausius <barcc@gmx.de>
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


from qt cimport *


ctypedef void CallbackErrorList(const QmlErrorList &) nogil


#### QtCore ####

#QList[QQmlError]
cdef extern from "<QtCore/QList>":
    cdef cppclass QmlErrorList "QList<QQmlError>":
        int size() const
        const QQmlError& at(int i) const
        
#QObject
cdef extern from "<QtCore/QObject>" namespace "QObject":
    Connection connect(const QQmlEngine *sender, CallbackErrorList *signal, CallbackErrorList slot) nogil
    

#### QtQml ####

#QQmlComponent
cdef extern from "<QtQml/QQmlComponent>":
    cdef cppclass QQmlComponent:
        QQmlComponent(QQmlEngine *, QObject *parent)
        void loadUrl(const QUrl &url)
        void loadUrl(const QUrl &url, CompilationMode mode)
        cbool isError() const
        cbool isLoading() const
        QmlErrorList errors() const
        QObject *beginCreate(QQmlContext *)
        void completeCreate()
cdef extern from "<QtQml/QQmlComponent>" namespace "QQmlComponent":
    cdef enum CompilationMode:
        PreferSynchronous, Asynchronous
        
#QQmlContext
cdef extern from "<QtQml/QQmlContext>":
    cdef cppclass QQmlContext:
        void setContextProperty(const QString &name, QObject *value)
        
#QQmlEngine
cdef extern from "<QtQml/QQmlEngine>":
    cdef cppclass QQmlEngine:
        QQmlEngine()
        void setOutputWarningsToStandardError(cbool enabled)
        QQmlContext *rootContext() const
        void clearComponentCache()
cdef extern from "<QtQml/QQmlEngine>" namespace "QQmlEngine":
    void warnings(const QmlErrorList &warnings) nogil
        
#QQmlError
cdef extern from "<QtQml/QQmlError>":
    cdef cppclass QQmlError:
        QString description() const
        QUrl url() const
        int line() const
        int column() const
        
        
#### QtQuick ####

#QQuickItem
cdef extern from "<QtQuick/QQuickItem>":
    cdef cppclass QQuickItem(QObject):
        qreal x() nogil const
        qreal y() nogil const
        qreal width() nogil const
        qreal height() nogil const
        QQuickItem *parentItem() nogil const
        QQuickWindow *window() nogil const
        
#QWindow
cdef extern from "<QtGui/QWindow>":
    cdef cppclass QWindow:
        QSurfaceFormat format() nogil const
        void resize(int w, int h) nogil
        cbool close() nogil
        int width() nogil const
        int height() nogil const
        cbool setMouseGrabEnabled(cbool grab) nogil
        #void setParent(QWindow *parent)
        void setTransientParent(QWindow * parent) nogil
#QQuickWindow
cdef extern from "<QtQuick/QQuickWindow>":
    cdef cppclass QQuickWindow(QWindow):
        void update() nogil
        void setCursor(const QCursor &cursor) nogil
        void unsetCursor() nogil
        void setPersistentOpenGLContext(cbool persistent) nogil
        void setClearBeforeRendering(cbool enabled) nogil
        QImage grabWindow()
        int x() nogil const
        int y() nogil const
        QQuickItem *contentItem() nogil const
        void setRenderTarget(QOpenGLFramebufferObject *fbo) nogil
#QQuickView
cdef extern from "<QtQuick/QQuickView>":
    cdef cppclass QQuickView(QQuickWindow):
        QQuickView(QQmlEngine *engine, QQuickWindow *parent)
        void setSource(const QUrl &url)
        void setTitle(const QString &title)
        QmlErrorList errors() const
        void setResizeMode(ResizeMode)
        void show()
cdef extern from "<QtQuick/QQuickView>" namespace "QQuickView":
    cdef enum ResizeMode:
        SizeViewToRootObject, SizeRootObjectToView
    void beforeSynchronizing() nogil
    void beforeRendering() nogil
        
        


