# generated with: tools/create-gl-pxd.py gl pybiklib/ext gl_ogl.pxd glarea.py gldraw.py gl_ogl.pxd

from libc.stddef cimport ptrdiff_t
from libc.stdint cimport int32_t, intptr_t, int8_t, uint8_t


# defines from /usr/include/GL/gl.h:

cdef extern from 'GL/gl.h':
    enum: GL_FALSE
    enum: GL_TRUE
    enum: GL_UNSIGNED_BYTE
    enum: GL_FLOAT
    enum: GL_POINTS
    enum: GL_LINES
    enum: GL_TRIANGLES
    enum: GL_CCW
    enum: GL_BACK
    enum: GL_CULL_FACE
    enum: GL_DEPTH_TEST
    enum: GL_RGB
    enum: GL_RGBA
    enum: GL_MAX_TEXTURE_SIZE
    enum: GL_TEXTURE_2D
    enum: GL_VENDOR
    enum: GL_RENDERER
    enum: GL_VERSION
    enum: GL_EXTENSIONS
    enum: GL_INVALID_ENUM
    enum: GL_INVALID_VALUE
    enum: GL_INVALID_OPERATION
    enum: GL_OUT_OF_MEMORY
    enum: GL_DEPTH_BUFFER_BIT
    enum: GL_COLOR_BUFFER_BIT
    enum: GL_MULTISAMPLE
    enum: GL_SAMPLE_ALPHA_TO_COVERAGE
    enum: GL_SAMPLE_COVERAGE
    enum: GL_SAMPLE_BUFFERS
    enum: GL_SAMPLES
    enum: GL_SAMPLE_COVERAGE_VALUE
    enum: GL_SAMPLE_COVERAGE_INVERT


# defines from /usr/include/GL/glext.h:

cdef extern from 'GL/glext.h':
    enum: GL_ARRAY_BUFFER
    enum: GL_STATIC_DRAW
    enum: GL_MAX_VERTEX_ATTRIBS
    enum: GL_FRAGMENT_SHADER
    enum: GL_VERTEX_SHADER
    enum: GL_DELETE_STATUS
    enum: GL_COMPILE_STATUS
    enum: GL_LINK_STATUS
    enum: GL_VALIDATE_STATUS
    enum: GL_INFO_LOG_LENGTH
    enum: GL_ATTACHED_SHADERS
    enum: GL_ACTIVE_UNIFORMS
    enum: GL_ACTIVE_UNIFORM_MAX_LENGTH
    enum: GL_ACTIVE_ATTRIBUTES
    enum: GL_ACTIVE_ATTRIBUTE_MAX_LENGTH
    enum: GL_SHADING_LANGUAGE_VERSION


# typedefs from /usr/include/GL/gl.h:

cdef extern from *:
    ctypedef unsigned int	GLenum
    ctypedef unsigned char	GLboolean
    ctypedef unsigned int	GLbitfield
    ctypedef void		GLvoid
    ctypedef int		GLint
    ctypedef unsigned char	GLubyte
    ctypedef unsigned int	GLuint
    ctypedef int		GLsizei
    ctypedef float		GLfloat
    ctypedef float		GLclampf


# typedefs from /usr/include/GL/glext.h:

cdef extern from *:
    ctypedef ptrdiff_t GLsizeiptr
    ctypedef ptrdiff_t GLintptr
    ctypedef char GLchar


# other typedefs:

cdef extern from *:
    ctypedef GLchar* const_GLchar_ptr "const GLchar*"


# functions from /usr/include/GL/gl.h:

cdef extern from 'GL/gl.h':
    cdef void  glClearColor( GLclampf red, GLclampf green, GLclampf blue, GLclampf alpha ) nogil
    cdef void  glClear( GLbitfield mask ) nogil
    cdef void  glCullFace( GLenum mode ) nogil
    cdef void  glFrontFace( GLenum mode ) nogil
    cdef void  glEnable( GLenum cap ) nogil
    cdef void  glDisable( GLenum cap ) nogil
    cdef GLboolean  glIsEnabled( GLenum cap ) nogil
    cdef void  glGetBooleanv( GLenum pname, GLboolean *params ) nogil
    cdef void  glGetFloatv( GLenum pname, GLfloat *params ) nogil
    cdef void  glGetIntegerv( GLenum pname, GLint *params ) nogil
    cdef GLenum  glGetError() nogil
    cdef GLubyte *  glGetString( GLenum name ) nogil
    cdef void  glViewport( GLint x, GLint y,
                                    GLsizei width, GLsizei height ) nogil
    cdef void  glDrawArrays( GLenum mode, GLint first, GLsizei count ) nogil
    cdef void  glReadPixels( GLint x, GLint y,
                                    GLsizei width, GLsizei height,
                                    GLenum format, GLenum type,
                                    GLvoid *pixels ) nogil
    cdef void  glTexImage2D( GLenum target, GLint level,
                                    GLint internalFormat,
                                    GLsizei width, GLsizei height,
                                    GLint border, GLenum format, GLenum type,
                                    GLvoid *pixels ) nogil
    cdef void  glTexSubImage2D( GLenum target, GLint level,
                                       GLint xoffset, GLint yoffset,
                                       GLsizei width, GLsizei height,
                                       GLenum format, GLenum type,
                                       GLvoid *pixels ) nogil
    cdef void  glActiveTexture( GLenum texture ) nogil


# functions from /usr/include/GL/glext.h:

cdef extern from 'GL/glext.h':
    cdef void  glBindBuffer (GLenum target, GLuint buffer) nogil
    cdef void  glDeleteBuffers (GLsizei n, GLuint *buffers) nogil
    cdef void  glGenBuffers (GLsizei n, GLuint *buffers) nogil
    cdef void  glBufferData (GLenum target, GLsizeiptr size, void *data, GLenum usage) nogil
    cdef void  glBufferSubData (GLenum target, GLintptr offset, GLsizeiptr size, void *data) nogil
    cdef void  glAttachShader (GLuint program, GLuint shader) nogil
    cdef void  glBindAttribLocation (GLuint program, GLuint index, GLchar *name) nogil
    cdef void  glCompileShader (GLuint shader) nogil
    cdef GLuint  glCreateProgram () nogil
    cdef GLuint  glCreateShader (GLenum type) nogil
    cdef void  glDeleteProgram (GLuint program) nogil
    cdef void  glDeleteShader (GLuint shader) nogil
    cdef void  glDetachShader (GLuint program, GLuint shader) nogil
    cdef void  glDisableVertexAttribArray (GLuint index) nogil
    cdef void  glEnableVertexAttribArray (GLuint index) nogil
    cdef void  glGetActiveAttrib (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) nogil
    cdef void  glGetActiveUniform (GLuint program, GLuint index, GLsizei bufSize, GLsizei *length, GLint *size, GLenum *type, GLchar *name) nogil
    cdef GLint  glGetAttribLocation (GLuint program, GLchar *name) nogil
    cdef void  glGetProgramiv (GLuint program, GLenum pname, GLint *params) nogil
    cdef void  glGetProgramInfoLog (GLuint program, GLsizei bufSize, GLsizei *length, GLchar *infoLog) nogil
    cdef void  glGetShaderiv (GLuint shader, GLenum pname, GLint *params) nogil
    cdef void  glGetShaderInfoLog (GLuint shader, GLsizei bufSize, GLsizei *length, GLchar *infoLog) nogil
    cdef GLint  glGetUniformLocation (GLuint program, GLchar *name) nogil
    cdef void  glLinkProgram (GLuint program) nogil
    cdef void  glShaderSource (GLuint shader, GLsizei count, GLchar **string, GLint *length) nogil
    cdef void  glUseProgram (GLuint program) nogil
    cdef void  glUniform1i (GLint location, GLint v0) nogil
    cdef void  glUniformMatrix4fv (GLint location, GLsizei count, GLboolean transpose, GLfloat *value) nogil
    cdef void  glValidateProgram (GLuint program) nogil
    cdef void  glVertexAttribPointer (GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, void *pointer) nogil

# GL version 2.0 needed

